jQuery(window).on('load', function () {
  jQuery("body").addClass("loaded");
  var counter = 0;
  var c = 0;
  var i = setInterval(function () {
      jQuery(".loading-page .counter h1").html(c + "%");
      jQuery(".loading-page .counter hr").css("width", c + "%");

      counter++;
      c++;

      if (counter == 101) {
          clearInterval(i);
          jQuery(".loading-page").fadeOut();
          jQuery("body").removeClass("loaded");
      }

  }, 0.5);
});
//Preloader
jQuery(document).ready(function () {
  AOS.init();
  
  (function ($) {
    'use strict';
    /*Header fix */
    setHeader();
    jQuery(window).scroll(function () {
      setHeader();
    });
    $(document).ready(function () {
      $('.clickme .tab-list').click(function () {
        $('.clickme .tab-list').removeClass('activelink');
        $(this).addClass('activelink');
        var tagid = $(this).data('tag');
        $('.virtual-tab').removeClass('active').addClass('hide');
        $('#' + tagid).addClass('active').removeClass('hide');
      });
    });


    function setHeader() {
      var head = jQuery("#sticky");
      var windowpos = jQuery(window).scrollTop();
      if (windowpos >= 50) {
        head.addClass("fixed-header");
      } else {
        head.removeClass("fixed-header");
      }
    }
    // Input FirstLetter Cap
    jQuery('.firstCap, textarea').on('keypress', function (event) {
      var jQuerythis = jQuery(this),
        thisVal = jQuerythis.val(),
        FLC = thisVal.slice(0, 1).toUpperCase(),
        con = thisVal.slice(1, thisVal.length);
      jQuery(this).val(FLC + con);
    });

    $(document).ready(function () {
      var owl = $('.clients-list');
      owl.owlCarousel({
        loop: true,
        center: true,
        nav: false,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          960: {
            items: 3
          },
          1200: {
            items: 3
          }
        }
      });
    })
    /* For Media Page LoadMore*/ 
    $(function () {
      $(".col-md-4").slice(0, 9).show();
      $(".media").on('click touchstart', '.load-more', function (e) {
        e.preventDefault();
        $(".col-md-4:hidden").slice(0, 3).slideDown();
        if ($(".col-md-4:hidden").length == 0) {
          $(".load-more").css('visibility', 'hidden');
        }
        $('.media').animate({
          scrollTop: $(this).offset().top
        }, 1000);
      });
    });
    //Avoid pinch zoom on iOS
    document.addEventListener('touchmove', function (event) {
      if (event.scale !== 1) {
        event.preventDefault();
      }
    }, false);
  })(jQuery)
});